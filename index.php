<?php
/*******************************************************************************
 * Copyright (c) 2015 ALL4TEC.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jonathan Dumont (ALL4TEC) - initial API and implementation
 ******************************************************************************/
?>
<?php
  # Global variables  
  $pageTitle    = "Eclipse Safety Framework";
  $pageKeywords = "Polarsys,ESF,Safety";
  $pageAuthor   = "Jonathan Dumont";

  include("theme/header.php");
  include("theme/menu.php");
?>
  <div id="body" class="width-started">
  <section id="content">
      <article class="expanded">
        <p>
          <b>Eclipse Safety Framework</b> provides a set of tools for integrating safety techniques within a model driven engineering process based on both modelling standards, SysML and MARTE.
        </p>
        <p>
          Model-Based Safety Analysis relies on the idea that safety analysis activities can follow the design process in a parallel flow using the system functional and physical architectures as a common basis. 
          The system model is used to capture the overall architectures and the interactions between their components. This abstract view of the system may be enriched with safety information using dedicated annotations 
          in order to describe possible dysfunctional behaviours.
        </p>
        <h3>Perform your safety analysis easily</h3>
        <p>
          As systems are becoming more complex, their safety assessment dramatically needs powerful tools. Most of 
          the existing tools are poorly connected to the system design process and cannot be associated at early stages of the development cycle. 
        </p>
        <p>  
          ESF will allow better interactivity between design and safety assessment activities. 
          A dysfunctional model is built from the system model. It is used to specify possible failure-modes, mitigation barriers and propagation 
          behaviour at components level. This is the manual local analysis. From the specification of feared events (expressed in safety requirements), 
          it can then with an automatic global analysis produce propagation paths and corresponding fault trees. The dysfunctional model can be 
          improved in an iterative way, until the safety requirements are fully satisfied. Finally, reports can be exported in different formats 
          (e.g. HTML and PDF) to document the analyses hypothesis and results.
        </p>
        <img class="center" alt="Overview" src="images/esf_intro.png">
        <p>
          Moreover, as this approach is based on models, each time the system model evolves, a new safety analysis can be done on the modified parts, 
          and keep the previous analysis on each unchanged component. This represents an important time-saving.
        </p>
        
        <h3>An open and standard DSML</h3>
        <p>
          To open our tools to end users customisation, and allow an interoperability with other tools, ESF uses a metamodel
          based on UML and profiled to comply with the safety domain. Another important objective of this project is thus the standardisation
          of its metamodel.  
        </p>
      </article>
    </section>

<?php
  include("sidebar.php");
?>

    <div class="clear"></div>
  </div>

<?php
  include("theme/footer.php");
?>
