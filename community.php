<?php
/*******************************************************************************
 * Copyright (c) 2015 ALL4TEC.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jonathan Dumont (ALL4TEC) - initial API and implementation
 ******************************************************************************/
?>
<?php
  # Global variables  
  $pageTitle    = "ESF Community";
  $pageKeywords = "Polarsys,ESF,Safety,Community";
  $pageAuthor   = "Jonathan Dumont";

  include("theme/header.php");
  include("theme/menu.php");
?>
  <div id="body" class="width-started">
    <section id="content">
      <article class="expanded">
        <p>
          Building a community is one of the main key to promote an Open Source project, and we want  
          to gather end users, contributors and academics to make a vivid community.  
        </p>

        <table border="0" cellspacing="0"; style="border-style:none; margin : 10px; width: 70%;">
          <tr>
            <td><a href="//polarsys.org/forums/index.php/f/15/"><img src="images/community/forum.png" alt="Forum" width="60"></a></td>
            <td>Ask your questions on the ESF forum.</td>
          </tr>
          <tr>
            <td><a href="//polarsys.org/bugs/describecomponents.cgi?product=ESF"><img src="images/community/buggie.png" alt="Bugzilla" width="60"></a></td>
            <td>Report a bug or an enhancement request on Bugzilla.</td>
          </tr>
          <tr>
            <td><a href="//polarsys.org/wiki/Kitalpha"><img src="images/community/wiki.png" alt="Wiki" width="60"></a></td>
            <td>Consult our Wiki for the development resources and guides.</td>
          </tr>
          <tr>
            <td><a href="//polarsys.org/mailman/listinfo/esf-dev"><img src="images/community/mailing.png" alt="Mailing-list" width="60"></a></td>
            <td>Subscribe to the developers mailing-list to follow technical discussions.</td>
          </tr>
        </table>

      </article>
    </section>

<?php
  include("sidebar.php");
?>

    <div class="clear"></div>
  </div>

<?php
  include("theme/footer.php");
?>
