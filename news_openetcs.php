<?php
/*******************************************************************************
 * Copyright (c) 2015 ALL4TEC.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jonathan Dumont (ALL4TEC) - initial API and implementation
 ******************************************************************************/
?>
<?php
  # Global variables  
  $pageTitle    = "OpenETCS";
  $pageKeywords = "Polarsys,ESF,OpenETCS";
  $pageAuthor   = "Jonathan Dumont";

  include("theme/header.php");
  include("theme/menu.php");
?>
  <div id="body" class="width-started">
    <section id="content">
      <article class="expanded">
        <h3>Issued by the OpenETCS Project</h3>
        <p>
          <a href="http://openetcs.org/" target="_blank"><b>OpenETCS</b></a> is a European project ITEA2 that aims at developing an integrated modeling, development, validation and testing framework 
          for leveraging the cost-efficient and reliable implementation of ETCS. The framework provides a holistic tool chain across the whole 
          development process of ETCS software. This tool chain supports the formal specification and verification of the ETCS system requirements, 
          the automatic and ETCS compliant code generation and validation, and the model-based test case generation and execution. 
        </p>
        <img src="images/news/ice.jpg" class="center" style="margin-bottom:15px;" alt="ICE">
        <p>
          <a href="http://openetcs.org/" target="_blank"><b>OpenETCS</b></a> uses 'Open Standards' on all levels, including hardware and software specification, interface definition, design tools, verification and validation procedures and 
          last but not least embedded control software. By applying those technologies and related business concepts a significant cost cut for the final 
          onboard product is expected down to or even below conventional high performance cab signaling systems (e.g. LZB Linien-Zug-Beeinflussung, 
          as used in Germany, Austria and Spain). The open source concept provides for a neutral and formal method based 'correct' functioning reference 
          device that will help to overcome existing interoperability problems, supporting manufacturers, infrastructure managers and railway undertakings alike, 
          avoiding exhaustive field tests, transferring verification and validation activities from the track site into laboratories, saving scarce resources 
          and finally accelerating the migration phase and therefore supporting the European ERTMS deployment plan.
        </p>
        <p>
          In the context of the <a href="http://openetcs.org/" target="_blank"><b>OpenETCS</b></a> project, <a href="http://www.all4tec.net/" target="_blank"><b>ALL4TEC</b></a> brought its skills on safety and validation to contribute to the demonstration of the SIL4 level 
          of CENELEC standard. The project gave us the opportunity to re-enforce the links between our Model Based Safety Analysis (MBSA) tool Safety Architect 
          and the SysML tool <a href="http://www.eclipse.org/papyrus/" target="_blank"><b>Papyrus</b></a> from the French Research entity <a href="http://www-list.cea.fr" target="_blank"><b>CEA LIST</b></a> (Comissariat à l’Energie Atomique). It was the starting point for the opensourcing 
          of this common project under the name of ESF.
        </p>
      </article>
    </section>

<?php
  include("sidebar.php");
?>

    <div class="clear"></div>
  </div>

<?php
  include("theme/footer.php");
?>
