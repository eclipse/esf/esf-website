<?php
/*******************************************************************************
 * Copyright (c) 2015 ALL4TEC.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jonathan Dumont (ALL4TEC) - initial API and implementation
 ******************************************************************************/
?>
    <aside class="sidebar">
      <ul>  
        <li>
          <h4>News</h4>
          <ul>
            <li>
              2015-03-22: <a href="https://www.eclipsecon.org/france2015/session/esf-birth-polarsys-tool-based-papyrus"><b>Ignite Talk</b></a> approved for the next EclipseCon France. 
            </li>
            <li>
              2015-03-06: <a href="//polarsys.org/wiki/esf"><b>Wiki</b></a> is now opened. 
            </li>
            <li>
              2015-02-27: ESF project has been created on Polarsys!
            </li>
          </ul>
        </li>
        <li>
          <br />
          <h4>About this project</h4>
          <ul>
            <li>
              Co-leaded by <a href="http://www.all4tec.net/" target="_blank"><b>ALL4TEC</b></a> and <a href="http://www-list.cea.fr" target="_blank"><b>CEA LIST</b></a>.
            </li>
            <li>
              Issued by the <a href="/esf/news_openetcs.php"><b>OpenETCS</b></a> project.
            </li>
            <li>
              Hosted by PolarSys. 
            </li>
            <li>
              Incubation phase.<br />
              <a href="//www.eclipse.org/projects/what-is-incubation.php"><img src="images/egg_incubation.png" alt="incubation"/></a>
            </li>
          </ul>
        </li>
      </ul>
    </aside>