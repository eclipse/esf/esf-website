<?php
/*******************************************************************************
 * Copyright (c) 2015 ALL4TEC.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jonathan Dumont (ALL4TEC) - initial API and implementation
 ******************************************************************************/
?>

<?php
  // Compute the menu style, to display the selected tab according to the current php file
  $filename       = basename($_SERVER['PHP_SELF']);
  $indexStyle     = "";
  $featuresStyle  = "";
  $startStyle     = "";
  $communityStyle = "";
  if ($filename == "index.php") {
    $indexStyle = "selected";
  } else if ($filename == "features.php") {
    $featuresStyle = "selected";
  } else if ($filename == "start.php") {
    $startStyle = "selected";
  } else if ($filename == "community.php") {
    $communityStyle = "selected";
  }
?>

<header>
  <div>
    <div class="logo-left width">
      <a href="index.php"><img src="images/logo_esf_full.png" alt="Eclipse Safety Framework"></a>
    </div>
  </div>
</header>

<nav>
  <div class="width">
    <ul>
      <li class="start <? echo $indexStyle?>"><a href="/esf/">Home</a></li>
      <li class="divider-vertical-second-menu"></li>
      <li class="<? echo $featuresStyle?>"><a href="/esf/workinprogress.php">Features</a></li>
      <li class="divider-vertical-second-menu"></li>
      <li class="<? echo $startStyle?>"><a href="/esf/workinprogress.php">Getting Started</a></li>
      <li class="divider-vertical-second-menu"></li>
      <li class="end <? echo $communityStyle?>"><a href="/esf/community.php">Community</a></li>
      <li style="text-align:right;"><a href="/esf/workinprogress.php" class="box">Download</a></li>
      <li style="float:none;"><br style="clear:both;" /></li> 
    </ul>    
  </div>
</nav>

<div id="hrdiv">
  <img src="images/bg_breadcrumb.jpg" width="" alt="" height="" />
</div>

<div id="body" class="width-started">
  <!--  Start of main content -->