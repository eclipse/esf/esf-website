<?php
/*******************************************************************************
 * Copyright (c) 2015 ALL4TEC.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jonathan Dumont (ALL4TEC) - initial API and implementation
 ******************************************************************************/
?>
  <!--  footer.php -->
  </div>

  <footer>
    <div class="footer-content width">
      <ul>
        <li><h4>PolarSys</h4></li>
        <li><a href="//www.polarsys.org/about-us">About</a></li>
        <li><a href="//www.polarsys.org/contact-us">Contact us</a></li>
        <li><a href="//www.polarsys.org/projects/polarsys.esf">ESF project page</a></li>
      </ul>
      <ul>
        <li><h4>Legal</h4></li>
        <li><a href="//www.eclipse.org/legal/privacy.php">Privacy policy</a></li>
        <li><a href="//www.eclipse.org/legal/termsofuse.php">Terms of use</a></li>
        <li><a href="//www.eclipse.org/legal/copyright.php">Copyright agent</a></li>
        <li><a href="//www.eclipse.org/org/documents/epl-v10.php">Eclipse public license</a></li>
        <li><a href="//www.eclipse.org/legal/">Legal resources</a></li>
      </ul>
      <ul>
        <li><h4>Useful links</h4></li>
        <li><a href="//polarsys.org/bugs/describecomponents.cgi?product=ESF">Report a bug</a></li>
        <li><a href="//polarsys.org/wiki/esf">How to contribute</a></li>
        <li><a href="//polarsys.org/mailman/listinfo/esf-dev">Mailing list</a></li>
        <li><a href="//polarsys.org/forums/index.php/f/15/">Forum</a></li>
      </ul>
      <ul class="endfooter">
        <li><h4>Other</h4></li>
        <li><a href="//www.all4tec.com/">ALL4TEC</a></li>
        <li><a href="//www-list.cea.fr/">CEA List</a></li>
        <li><a href="//www.eclipse.org/org/workinggroups/">Working Groups</a></li>
      </ul>
      <table>
        <tr>
          <td>
            <a href="//www.polarsys.org/"><img src="images/polarsys.png" alt="PolarSys" width="250"></a>
            <br />
            <br />
          </td>
          <td>
            <div class="right-footer">
              <a href="//www.eclipse.org/"><img src="images/eclipse.png" alt="Eclipse logo"></a>
              <br />
              Copyright © <span id="year">2014</span> The Eclipse Foundation. <br /> All Rights Reserved.
            </div>
          </td>
        </tr>
      </table>

      <div class="clear"></div>
    </div>

  </footer>
</body>
</html>