<?php
/*******************************************************************************
 * Copyright (c) 2015 ALL4TEC.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jonathan Dumont (ALL4TEC) - initial API and implementation
 ******************************************************************************/
?>
<?php
  # Global variables  
  $pageTitle    = "Eclipse Safety Framework WIP";
  $pageKeywords = "Polarsys,ESF,Safety";
  $pageAuthor   = "Jonathan Dumont";

  include("theme/header.php");
  include("theme/menu.php");
?>
  <div id="body" class="width-started">
  <section id="content">
      <article class="expanded">
        <p>
          Work in progress...
        </p>
      </article>
    </section>

<?php
  include("sidebar.php");
?>

    <div class="clear"></div>
  </div>

<?php
  include("theme/footer.php");
?>

